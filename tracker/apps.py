from __future__ import unicode_literals

from django.apps import AppConfig

"""
REST API for expense tracking app.
"""
class ExpenseTrackerConfig(AppConfig):
    name = 'tracker'
