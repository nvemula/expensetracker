from rest_framework import generics
from django.contrib.auth.models import User
from tracker.serializers import ExpenseSerializer
from tracker.serializers import Expense
from tracker.permissions import IsOwnerOrAdmin, IsOwner
from rest_framework import permissions
from rest_framework import mixins
import django_filters
from rest_framework.authentication import BasicAuthentication
from expenset.csrf_check import *


class ExpenseFilter(django_filters.rest_framework.FilterSet):
    # Expense filter for date ranges
    min_date = django_filters.DateFilter(name="created", lookup_expr='gte')
    max_date = django_filters.DateFilter(name="created", lookup_expr='lte')
    class Meta:
        model = Expense
        fields = ['min_date', 'max_date']

class ExpenseList(generics.ListCreateAPIView):
    """
    List all expenses, or create a new expense.
    """
    queryset = Expense.objects.all()
    serializer_class = ExpenseSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrAdmin)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_class = ExpenseFilter
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get_queryset(self):
        if self.request.user.is_staff:
            return Expense.objects.all()
        else:
            return Expense.objects.filter(created_by=self.request.user)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

class ExpenseReport(generics.ListAPIView):
    """
    List all expenses for report.
    """
    serializer_class = ExpenseSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_class = ExpenseFilter
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get_queryset(self):
        return Expense.objects.filter(created_by=self.request.user)

class ExpenseDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete an expense.
    """
    queryset = Expense.objects.all()
    serializer_class = ExpenseSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrAdmin)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
