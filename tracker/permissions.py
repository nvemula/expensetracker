from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """
    Permission for PUT, DELETE, GET, POST, PATCH an expense by a user.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are only allowed to the owner of the expense.
        return obj.created_by == request.user

class IsOwnerOrAdmin(permissions.BasePermission):
    """
    Permission for PUT, DELETE, GET, POST, PATCH an expense by a user.
    """

    def has_object_permission(self, request, view, obj):
        # SAFE METHODS are read, head and options
        if request.method in permissions.SAFE_METHODS:
            # Admin can read everyone's expenses
            if request.user.is_staff:
                return True
            # A regular user can only read his own expeses
            elif obj.created_by==request.user:
                return True
            # Non authenticated cannot read anything
            else:
                return False

        # Write permissions are only allowed to the owner of the expense.
        return obj.created_by == request.user
