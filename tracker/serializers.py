from django.contrib.auth.models import User
from rest_framework import serializers
from tracker.models import Expense

"""
Expense Serializer for checking validity for REST calls.
These are similar to forms in normal django applications
"""

class ExpenseSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    description = serializers.CharField(required=True, allow_blank=False, max_length=256)
    created_by = serializers.ReadOnlyField(source='created_by.id')
    created = serializers.DateTimeField(required=False)
    amount = serializers.DecimalField(max_digits=7, decimal_places=2)

    def create(self, validated_data):
        """
        Create and return a new `Expense` instance, given the validated data.
        """
        return Expense.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Expense` instance, given the validated data.
        """
        instance.description = validated_data.get('description', instance.description)
        instance.amount = validated_data.get('amount', instance.amount)
        instance.save()
        return instance

    class Meta:
        model = Expense
        fields = ('id', 'description', 'amount', 'created')
