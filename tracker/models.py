from __future__ import unicode_literals

from django.db import models

"""
Expense model
"""
class Expense(models.Model):
    description = models.CharField(max_length=256, blank=False, default='')
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    created = models.DateTimeField(blank=True, auto_now_add = True)
    created_by = models.ForeignKey('auth.User', related_name='expenses', on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)
